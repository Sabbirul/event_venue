<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Venue;

class VenueController extends Controller
{
    //
    public function list(){
        $vanue_list= Venue::all();
        return view('vanue_list',compact('vanue_list'));  
    }

    public function apilist(){
        return Venue::all();
    }
    //
    public function admin_venue_list(){
        $vanue_list= Venue::all();
        return view('venue_list',compact('vanue_list'));  
    }

    public function add(Request $request){
        $result=Venue::create($request->all());
        if($result){
			return redirect()->route('admin_venue_list');
		}
		else{
            return redirect()->route('add_venue_form');
		}
    }

    public function add_venue_form(){
        return view('add_venue');
    }

    public function view(){

    }

    public function show(){

    }

    public function edit(){

    }

    public function update(){

    }

    public function delete(){

    }
}
