<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\VenueController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homepage');
});
Route::get('/venue_show/{id}', [VenueController::class, 'show']);
Route::get('/venue_list', [VenueController::class, 'list']);

Route::get('/home', function () {
    return view('home');
});

Route::get('/switch/{locale}', function ($locale) {
    App::setLocale($locale);
    return view('homepage');
});

Auth::routes();
Route::get('/add_venue_form', [VenueController::class, 'add_venue_form'])->name('add_venue_form');
Route::post('/add_venue', [VenueController::class, 'add'])->name('add_venue');
Route::get('/admin_venue_list', [VenueController::class, 'admin_venue_list'])->name('admin_venue_list');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
